clear all; close all

% script for Homework 8 exercise 1:
% comparing iterative methods
% Jacobi method and the Gauss-Seidel method.
%
% Created by Cory R Robinson on 11/25/2013
%


max_iters = 400;
tol = 10^(-8);

% Linear systems:
randn('state',1)
m = 200;
A = 2*eye(m) + 0.5*randn(m)/sqrt(m);
b = ones(m,1);

n = size(A,2);
offDiag_A = A - diag(diag(A));

xj = zeros(m,1);  % initial guess
% Jacobi Method-----------------------------------------------------------
count_J = 0;
for k = 1:m
    for i = 1:n
        u(i) = (b(i) - sum(offDiag_A(i,:)*xj(:)))/A(i,i);
    end
    xj = u.';
    count_J = count_J + 1;
    if norm(b - A*xj) <= tol*norm(b)
        break
    elseif count_J == max_iters
        break
    end
end
 
iter_jacobi = count_J
x_jacobi = xj;
save iter_jacobi.dat iter_jacobi -ascii
save x_jacobi.dat x_jacobi -ascii



xG = zeros(m,1);   % reset initial guess
% Gauss-Seidel Method-----------------------------------------------------
count_G = 0;
for k = 1:m
    for i = 1:n
        xG(i) = (b(i) - sum(offDiag_A(i,:)*xG(:)))/A(i,i);
    end
    count_G = count_G + 1;
    if norm(b - A*xG) <= tol*norm(b)
        break
    elseif count_G == max_iters
        break
    end
end

iter_gauss = count_G
x_gauss = xG;
save iter_gauss.dat iter_gauss -ascii
save x_gauss.dat x_gauss -ascii
