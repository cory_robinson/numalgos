clear all; close all

% script on iterative refinement of the solution of Ax = b.
% this script iteratively returns better and better approximations of the
% solution to Ax = b by adding the small amout z to the solution x, where z
% is the solution to Az = r with r the residual vector.  Also this will
% result in the residual converging towards zero.
%
% created by Cory R Robinson on 11/4/2013
%

tol = 10^(-7);
A = [21 67 88 73; 76 63 7 20; 0 85 56 54; 19.3 43 30.2 29.4];
b = [141; 109; 218; 93.7];

format long

As = single(A);
bs = single(b);
[Ls, Us] = lu(As);              % LU factorization of A in single precision
L = double(Ls);                 % storing L in double precision
U = double(Us);
x1 = Us\(Ls\b);                 % solving Ax = b an approximate soln
x1_d = double(x1);

r = single(b - (A*x1_d));  % computing r in double precision
%rs = single(rd);                % storing r in single precision
z = Us\(Ls\r);                 % solving Az = r
x = x1;                         % 
soln = x + z;                     % improved approximation of x1
soln_d = double(soln);

for i = 1:10
    r = single(b - (A*soln_d))
    %rs = single(rd)
    z = Us\(Ls\r);
    soln = soln + z;
    soln_d = double(soln);
    error = abs(r-0);
    if error < tol
        solves = i+1;   % counting loop iterations + soln before loop
        break
    end
end

xfinal = soln_d;

save Ls.dat L -ascii -double
save x1.dat x1_d -ascii -double
save xfinal.dat xfinal -ascii -double
save solves.dat solves -ascii
