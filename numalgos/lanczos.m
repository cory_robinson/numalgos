clear all; close all

% script implementing the Lanczos Iteration
% based on Algorithm 36.1 in Trefethen and Bau
%
% Created by Cory Robinson on 12/2/2013
%

randn('state',1);
m = 200;
A = 2*eye(m) + 0.5*randn(m)/sqrt(m);
A = A.'*A;
b = ones(m,1);

beta(:,1) = 0;
q(:,1) = zeros(m,1);
q(:,2) = b/norm(b);
for n = 2:11
    v = A*q(:,n);
    alpha(:,n) = q(:,n)'*v;
    v = v - beta(:,n-1)*q(:,n-1) - alpha(:,n)*q(:,n);
    beta(:,n) = norm(v);
    q(:,n+1) = v/beta(:,n);
end


% delete initial values to get right dimensions for T
alpha(1) = [];
beta(11) = []; beta(1) = [];
T = diag(alpha) + diag(beta,-1) + diag(beta,1);    % T is 10x10
q(:,1) = [];    % now q is 200x11

% just for checking that T is correct:
QTAQ = q.'*A*q;
size(QTAQ);

a2 = A;
t10 = T;
q10 = q;
save a2.dat a2 -ascii
save t10.dat t10 -ascii
save q10.dat q10 -ascii