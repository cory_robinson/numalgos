# This is my README
----------------------
----------------------
This repo contains various numerical algorithms implemented in MATLAB.  Most of these algorithms are based on methods described in Trefethen and Bau's Numerical Linear Algebra.
----------------------

-The only numerical methods which aren't stated as a filename are in the iterative_methods_comp.m file. This file contains implementations of the Jacobi iteration and the Gauss Seidel methods, and the script compares the two methods, hence the name.

-All other file names reflect the numerical method implemented.
# This is my README
This repo contains various numerical algorithms implemented in MATLAB.  Most of these algorithms are based on methods described in Trefethen and Bau's Numerical Linear Algebra.  -The only numerical methods which aren't stated as a filename are in the iterative_methods_comp.m file. This file contains implementations of the Jacobi iteration and the Gauss Seidel methods, and the script compares the two methods, hence the name.
