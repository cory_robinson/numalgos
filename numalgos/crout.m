clear all; close all

% script that implements the Crout matrix factorization.
% Turn this into a function to call on any general square matrix.
%
% Created by Cory R Robinson on 11/3/2013
%

load MatrixB.dat

A = MatrixB;
size = size(A);
m = size(1)


%L = A
L(:,1) = A(:,1);
U = eye(m);
U(1,:) = A(1,:)/L(1,1);
u1 = U;


for k = 2:m
    for j = 2:k
        L(k,j) = A(k,j) - L(k,1:j-1)*U(1:j-1,j);
    end
    for j = k+1:m
        U(k,j) = (A(k,j) - L(k,1:k-1)*U(1:k-1,j))/L(k,k);
    end
    if k == 2
        u2 = U;
    end
end

lfinal = L
ufinal = U
save u1.dat u1 -ascii
save u2.dat u2 -ascii
save lfinal.dat lfinal -ascii
save ufinal.dat ufinal -ascii

L*U
MatrixB