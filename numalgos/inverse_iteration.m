clear all; close all;

% script for the Inverse Iteration method for finding eigenvalues
% based on Algorithm 27.2 in Trefethen & Bau
% 
% Created by Cory R Robinson on 11/16/2013
%

A = [6 2 1; 2 3 1; 1 1 1];
[m,n] = size(A);
v0 = [0;1;0];
v = v0;
I = eye(m);
tol = 10^(-6);


mu = 2;  %v0.'*A*v0;   % estimating the first eigenvalue (Trying 2 is better)
r0 = A*v - mu*v;  % residual for first eigenvalue
normr0 = norm(r0);  % norm of first residual

format long
for k = 1:1000
    w = (A-mu*I)^(-1)*v;    % algorithm says use this
    %w = (A-mu*I)\v;  used this one in submission       
    v = w/norm(w);
    lambda = v.' * A*v;
    
    r = A*v - lambda*v;
    normresidual(k) = norm(r);
    %mu
    if norm(r) < tol
        count = k;
        break
    end
end

%eig(A);
eigenvalue = lambda;
eigenvector = -1*v;
normresidual = [normr0 normresidual(1:4)].'

save eigenvalue.dat eigenvalue -ascii
save eigenvector.dat eigenvector -ascii
save normresidual.dat normresidual -ascii