clear all; close all;

% script for a simple QR factorization of a matrix
% 
% Created by Cory R Robinson on 11/16/2013
%

load MatrixB.dat;
% testing general matrices
%MatrixB = gallery('lehmer',15); %randi([-10,10],4,4);
B = MatrixB;

[m,n] = size(B);
epsilon = 10^(-4);

ndB = B(~eye(m,n));  % nondiagonal entries of B
%reshape(ndB,2,3);  % B without diagonal entries
dB = diag(B);   % diagonal entries of B

iterb = 0;
while max(abs(ndB)) > epsilon*max(abs(dB))
    [Q,R] = qr(B);
    B = R*Q;
    iterb = iterb + 1;
    ndB = B(~eye(m,n));  % nondiagonal entries of B
    dB = diag(B);   % diagonal entries of B
end

iterb
diagb = diag(B)
eig(MatrixB)

%sdb = sort(diagb)
%emb = eig(MatrixB)
%norm(abs(sdb - emb)) < epsilon

save diagb.dat diagb -ascii
save iterb.dat iterb -ascii

