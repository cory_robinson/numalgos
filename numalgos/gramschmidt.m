clear all; close all

% code for the classical Gram-Schmidt (unstable) algorithm
% based off of Algorithm 7.1 pg 51 of T&B
%
% Created by Cory R Robinson on 10/11/2013
%

%{
for j = 1 to n
    v_j = a_j
    for i = 1 to j-1
        r_ij = q*_i(a_j)
        v_j = v_j - r_ij(q_i)
    end
    r_jj = ||v_j||_2
    q_j = v_j / r_jj
end
%}

% part (a) and part (b):

A = [2 -1 0; 1 6 -2; 4 -3 8];
n = rank(A);     % assuming A has full rank

for j = 1:n         % use n for matrix A; use m for MatrixB
    v = A(:,j);     % v is the columns of A
    for i = 1:j-1
        Ra(i,j) = Qa(:,i)' * A(:,j);      
        v = v - Ra(i,j)*Qa(:,i);
    end
    Ra(j,j) = norm(v);
    Qa(:,j) = v/Ra(j,j);
end

%Qa
%Ra

%Qa*Ra == A

% part (c) with MatrixB:

load MatrixB.dat;
m = rank(MatrixB);      % rank of MatrixB

for j = 1:m         % use n for matrix A; use m for MatrixB
    v = MatrixB(:,j);     % v is the columns of A
    for i = 1:j-1
        Rb(i,j) = Qb(:,i)' * MatrixB(:,j);      
        v = v - Rb(i,j)*Qb(:,i);
    end
    Rb(j,j) = norm(v);
    Qb(:,j) = v/Rb(j,j);
end

%Qb*Rb == MatrixB


save qa.dat Qa -ascii
save ra.dat Ra -ascii
save qb.dat Qb -ascii
save rb.dat Rb -ascii