clear all; close all

% script for implementing the Arnoldi Iteration Algorithm.
% Based on Algorithm 33.1 in Trefethen and Bau
%
% Created by Cory R Robinson on 11/25/2013
%

randn('state',1)
m = 200;
A = 2*eye(m) + 0.5*randn(m)/sqrt(m);
b = ones(m,1);

% Arnoldi Method-----------------------------------------------------------
q = b/norm(b);
for n = 1:10
    v = A*q(:,n);
    for j = 1:n
        h(j,n) = q(:,j)'*v;
        v = v - h(j,n)*q(:,j);
    end
    h(n+1,n) = norm(v);
    if h(n+1,n) == 0
        break
    end
    q(:,n+1) = v/h(n+1,n);
end

a = A;
h10 = h;
%size(h10)
q10 = q;
%size(q10)
save a.dat a -ascii
save h10.dat h10 -ascii
save q10.dat q10 -ascii