clear all; close all;

% script for gmres method; homework 9
% based on Algorithms 33.1 and 35.1 in Trefethen and Bau
%
% Created by Cory Robinson on 12/1/2013
%

randn('state',1)
m = 200;
A = 2*eye(m) + 0.5*randn(m)/sqrt(m);
b = ones(m,1);

tol = 10^(-10);
xj = zeros(m,1);  % initial guess
q = b/norm(b);
iters = 0;
for n = 1:1000
    % BEGIN ARNOLDI ITERATION
    v = A*q(:,n);
    for j = 1:n
        h(j,n) = q(:,j)'*v;
        v = v - h(j,n)*q(:,j);
    end
    h(n+1,n) = norm(v);
    %if h(n+1,n) == 0
    %    break
    %end
    q(:,n+1) = v/h(n+1,n);
    % END ARNOLDI ITERATION
    
    % Do "minimized residual" part of the algorithm
    % Find y to minimize norm(h_10*e_1 - H_k*y_k) = norm(r_n)
    e1 = [1; zeros(n,1)];
    y = h\(norm(b)*e1);
    x = q(:,1:n)*y;
    iters = iters + 1;
    % Stopping criterion
    if norm(b - A*x) <= tol*norm(b)
        break
    end
end

a = A;
%x
%size(h)
%iters
save a.dat a -ascii
save x.dat x -ascii
save h.dat h -ascii
save iter.dat iters -ascii