clear all; close all

% Power Iteration Algorithm for finding largest eigenvalue 

A = [2 3 2; 10 3 4; 3 6 1];
v0 = [0;0;1];
[m,n] = size(A);
tol = 10^(-6);
v = v0;
eig(A);
nv = norm(v);
lambda0 = v0.'*A*v0;
r0 = A*v0 - lambda0*v0;
normr0 = norm(r0)

for k = 1:1000
    w = A*v;  % where v = v0 the initial value
    v = w/norm(w);
    lambda = v.'*A*v;
    r = A*v - lambda*v;
    normresidual(k) = norm(r);
    %v = v(k);
    if norm(r) < tol
        count = k;
        break
    end
end
count;
normresidual = [normr0 normresidual(1:4)]
eigenvalue = lambda
eigenvector = v

save eigenvalue.dat eigenvalue -ascii
save eigenvector.dat eigenvector -ascii
save normresidual.dat normresidual -ascii