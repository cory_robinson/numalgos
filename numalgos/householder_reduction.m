clear all; close all

% script for Householder reduction to Hessenberg form

load MatrixB.dat
B = MatrixB;
[m, n] = size(B);
e = 1; 
z = zeros(1,m-1);
e1 = [e z].';



for k = 1:m-2
    e1(end) = [];
    x = B(k+1:m,k);
    v = sign(x(1))*norm(x)*e1 + x;
    v = v/norm(v);
    if k == 1
        reflector1 = v;     % Householder reflector of norm 1
    end
    if k == 2
        reflector2 = v;     % Householder reflector of norm 1
    end
    B(k+1:m,k:m) = B(k+1:m,k:m) - 2*v*(v'*B(k+1:m,k:m));
    B(1:m,k+1:m) = B(1:m,k+1:m) - 2*(B(1:m,k+1:m)*v)*v';
    
end


v1 = reflector1;
v2 = reflector2;
%norm(v1)
%norm(v2)
%hess(B)
h = B;      % the final Hessenberg matrix
save v1.dat v1 -ascii
save v2.dat v2 -ascii
save h.dat h -ascii
